package com.haud.test.data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created By: Vvek
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class SimCard {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String details;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "simCards")
    private Customer customer;
}
