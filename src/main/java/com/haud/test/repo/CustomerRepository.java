package com.haud.test.repo;

import com.haud.test.data.Customer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created By: Vvek
 */
@Repository
public interface CustomerRepository extends CrudRepository<Customer, Integer> {
}
