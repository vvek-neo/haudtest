package com.haud.test.repo;

import com.haud.test.data.SimCard;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created By: Vvek
 */
@Repository
public interface SimCardRepository extends CrudRepository<SimCard, Integer> {
}
