package com.haud.test.service;

import com.haud.test.data.SimCard;

import java.util.Optional;

/**
 * Created By: Vvek
 */
public interface SimCardService {
    SimCard save(SimCard sim);

    SimCard findById(int id);
}
