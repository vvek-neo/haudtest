package com.haud.test.service;

import com.haud.test.data.SimCard;
import com.haud.test.repo.SimCardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created By: Vvek
 */
@Service
public class SimCardServiceImpl implements SimCardService {

    private final SimCardRepository simCardRepository;

    @Autowired
    public SimCardServiceImpl(SimCardRepository simCardRepository) {
        this.simCardRepository = simCardRepository;
    }

    @Override
    public SimCard save(SimCard sim) {
        return simCardRepository.save(sim);
    }

    @Override
    public SimCard findById(int id) {
        return simCardRepository.findById(id).orElse(null);
    }
}
