package com.haud.test.service;

import com.haud.test.data.Customer;
import com.haud.test.data.SimCard;
import com.haud.test.repo.CustomerRepository;
import com.haud.test.util.HasLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;

import java.util.List;

/**
 * Created By: Vvek
 */
@Service
public class CustomerServiceImpl implements CustomerService, HasLogger {

    private final CustomerRepository customerRepository;
    private final SimCardService simCardService;

    @Autowired
    public CustomerServiceImpl(CustomerRepository customerRepository, SimCardService simCardService) {
        this.customerRepository = customerRepository;
        this.simCardService = simCardService;
    }

    @Override
    public Customer save(Customer customer) {
        return customerRepository.save(customer);
    }

    @Override
    public Customer linkSimCard(int customerId, int simId) {
        //Retrieve
        Customer customer = customerRepository.findById(customerId).orElse(null);
        SimCard sim = simCardService.findById(simId);
        if (customer == null) {
            String msg = "Customer resource not found for id=" + customerId;
            getLogger().error(msg);
            throw new ResourceNotFoundException(msg);
        }
        if (sim == null) {
            String msg = "Sim resource not found for id=" + simId;
            getLogger().error(msg);
            throw new ResourceNotFoundException(msg);
        }
        if (sim.getCustomer() != null) {
            String msg = "sim " + simId + " has already been allocated";
            getLogger().warn(msg);
            throw new ResourceAccessException(msg);
        }
        //Link
        List<SimCard> simCards = customer.getSimCards();
        if (!simCards.contains(sim)) {
            simCards.add(sim);
            customer.setSimCards(simCards);
            customer = this.save(customer);

            sim.setCustomer(customer);
            simCardService.save(sim);
        }
        //Update
        return customer;
    }

    @Override
    public List<SimCard> getSims(int id) {
        Customer customer = customerRepository.findById(id).orElse(null);
        if (customer == null) {
            String msg = "Customer resource not found for id=" + id;
            getLogger().error(msg);
            throw new ResourceNotFoundException(msg);
        }
        return customer.getSimCards();
    }
}
