package com.haud.test.service;

import com.haud.test.data.Customer;
import com.haud.test.data.SimCard;

import java.util.List;

/**
 * Created By: Vvek
 */
public interface CustomerService {
    Customer save(Customer customer);

    Customer linkSimCard(int customerId, int simId);

    List<SimCard> getSims(int id);
}
