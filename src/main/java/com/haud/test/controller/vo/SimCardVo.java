package com.haud.test.controller.vo;

import com.haud.test.data.SimCard;
import lombok.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created By: Vvek
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class SimCardVo {
    private int id;
    private String details;

    public static SimCardVo map(SimCard simCard) {
        return new SimCardVo(simCard.getId(), simCard.getDetails());
    }

    public static List<SimCardVo> map(List<SimCard> simCardList) {
        return simCardList.stream().map(SimCardVo::map).collect(Collectors.toList());
    }
}
