package com.haud.test.controller.vo;

import lombok.*;

/**
 * Created By: Vvek
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ResponseVo<T> {
    private int code;
    private String message;
    private T data;
}
