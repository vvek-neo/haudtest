package com.haud.test.controller.vo;

import com.haud.test.data.Customer;
import lombok.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created By: Vvek
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CustomerVo {
    private int id;
    private String name;
    private List<SimCardVo> simCards = new ArrayList<>();

    public static CustomerVo map(Customer customer) {
        return new CustomerVo(customer.getId(), customer.getName(), SimCardVo.map(customer.getSimCards()));
    }

    public static List<CustomerVo> map(List<Customer> customerList) {
        return customerList.stream().map(CustomerVo::map).collect(Collectors.toList());
    }
}
