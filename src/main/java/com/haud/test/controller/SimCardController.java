package com.haud.test.controller;

import com.haud.test.controller.vo.ResponseVo;
import com.haud.test.controller.vo.SimCardVo;
import com.haud.test.data.SimCard;
import com.haud.test.service.SimCardService;
import com.haud.test.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created By: Vvek
 */
@RestController
@RequestMapping("/api/v1/sim")
public class SimCardController {

    private final SimCardService simCardService;

    @Autowired
    public SimCardController(SimCardService simCardService) {
        this.simCardService = simCardService;
    }

    @PostMapping(value = "", consumes = "application/json", produces = "application/json")
    public ResponseEntity<ResponseVo<SimCardVo>> createSim(@RequestBody SimCard sim) {
        SimCard saved = simCardService.save(sim);
        return new ResponseEntity<>(new ResponseVo<>(
                HttpStatus.OK.value(),
                Constants.RESPONSE_SUCCESS,
                SimCardVo.map(saved)), HttpStatus.OK);
    }
}
