package com.haud.test.controller.relation;

import lombok.*;

/**
 * Created By: Vvek
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class SimCustomerRelation {
    private int customerId;
    private int simId;
}
