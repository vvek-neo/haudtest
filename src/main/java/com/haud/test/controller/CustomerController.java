package com.haud.test.controller;

import com.haud.test.controller.relation.SimCustomerRelation;
import com.haud.test.controller.vo.CustomerVo;
import com.haud.test.controller.vo.ResponseVo;
import com.haud.test.controller.vo.SimCardVo;
import com.haud.test.data.Customer;
import com.haud.test.data.SimCard;
import com.haud.test.service.CustomerService;
import com.haud.test.util.Constants;
import com.haud.test.util.HasLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created By: Vvek
 */
@RestController
@RequestMapping("/api/v1/customer")
public class CustomerController implements HasLogger {

    private final CustomerService customerService;

    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @PostMapping(value = "", consumes = "application/json", produces = "application/json")
    public ResponseEntity<ResponseVo<CustomerVo>> createCustomer(@RequestBody Customer customer) {
        getLogger().debug("Create customer request for: " + customer);
        Customer saved = customerService.save(customer);
        return new ResponseEntity<>(new ResponseVo<>(
                HttpStatus.OK.value(),
                Constants.RESPONSE_SUCCESS,
                CustomerVo.map(saved)), HttpStatus.OK);
    }

    @PutMapping(value = "/linkSim", consumes = "application/json", produces = "application/json")
    public ResponseEntity<ResponseVo<CustomerVo>> linkSimToCustomer(@RequestBody SimCustomerRelation mapper) {
        getLogger().info("Sim link request for: " + mapper);
        Customer customer = customerService.linkSimCard(mapper.getCustomerId(), mapper.getSimId());
        return new ResponseEntity<>(new ResponseVo<>(
                HttpStatus.OK.value(),
                Constants.RESPONSE_SUCCESS,
                CustomerVo.map(customer)), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}/sims", produces = "application/json")
    public ResponseEntity<ResponseVo<List<SimCardVo>>> getSims(@PathVariable(value = "id") int id) {
        getLogger().debug("Sim list requested for customer id: " + id);
        List<SimCard> sims = customerService.getSims(id);
        return new ResponseEntity<>(new ResponseVo<>(
                HttpStatus.OK.value(),
                Constants.RESPONSE_SUCCESS,
                SimCardVo.map(sims)), HttpStatus.OK);
    }
}
