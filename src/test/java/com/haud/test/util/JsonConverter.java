package com.haud.test.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.haud.test.controller.vo.CustomerVo;
import com.haud.test.controller.vo.ResponseVo;
import com.haud.test.controller.vo.SimCardVo;

import java.io.IOException;
import java.util.List;

/**
 * Utility To Help with JSON Conversation
 * Created By: Vvek
 */
public class JsonConverter {
    private static final ObjectMapper mapper = new ObjectMapper();

    public static String convert(Object object) throws JsonProcessingException {
        return mapper.writeValueAsString(object);
    }

    public static ResponseVo<CustomerVo> convertToCustomer(String jsonString) throws IOException {
        return mapper.readValue(jsonString, new TypeReference<ResponseVo<CustomerVo>>() {
        });
    }

    public static ResponseVo<SimCardVo> convertToSimCard(String jsonString) throws IOException {
        return mapper.readValue(jsonString, new TypeReference<ResponseVo<SimCardVo>>() {
        });
    }

    public static ResponseVo<List<SimCardVo>> convertToSims(String jsonString) throws IOException {
        return mapper.readValue(jsonString, new TypeReference<ResponseVo<List<SimCardVo>>>() {
        });
    }
}
