package com.haud.test.controller;

import com.haud.test.controller.vo.ResponseVo;
import com.haud.test.controller.vo.SimCardVo;
import com.haud.test.service.CustomerService;
import com.haud.test.util.Constants;
import com.haud.test.util.JsonConverter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 * Created By: Vvek
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SimCardControllerTest {
    private MockMvc mvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private CustomerService customerService;

    @Before
    public void init() {
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void givenSimCardObjectAsJsonReturnPersistedObject() throws Exception {
        String uri = "/api/v1/sim";
        String details = "SimCardDetails";
        MockHttpServletResponse response = mvc.perform(MockMvcRequestBuilders
                .post(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"details\":\"" + details + "\"}"))
                .andReturn()
                .getResponse();

        ResponseVo<SimCardVo> responseVo = JsonConverter.convertToSimCard(response.getContentAsString());
        Assert.assertEquals(responseVo.getCode(), HttpStatus.OK.value());
        Assert.assertEquals(responseVo.getMessage(), Constants.RESPONSE_SUCCESS);
        Assert.assertEquals(responseVo.getData().getId(), 1);   //First Created
        Assert.assertEquals(responseVo.getData().getDetails(), details);
    }

}
