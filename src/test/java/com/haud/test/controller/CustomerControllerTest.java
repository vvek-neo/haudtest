package com.haud.test.controller;

import com.haud.test.controller.vo.CustomerVo;
import com.haud.test.controller.vo.ResponseVo;
import com.haud.test.controller.vo.SimCardVo;
import com.haud.test.data.Customer;
import com.haud.test.data.SimCard;
import com.haud.test.service.CustomerService;
import com.haud.test.util.Constants;
import com.haud.test.util.JsonConverter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created By: Vvek
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerControllerTest {
    private MockMvc mvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private CustomerService customerService;

    @Before
    public void init() {
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void givenCustomerObjectAsJsonReturnPersistedObject() throws Exception {
        String uri = "/api/v1/customer";
        String name = "John";
        MockHttpServletResponse response = mvc.perform(MockMvcRequestBuilders
                .post(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"" + name + "\"}"))
                .andReturn()
                .getResponse();

        ResponseVo<CustomerVo> responseVo = JsonConverter.convertToCustomer(response.getContentAsString());
        Assert.assertEquals(responseVo.getCode(), HttpStatus.OK.value());
        Assert.assertEquals(responseVo.getMessage(), Constants.RESPONSE_SUCCESS);
        Assert.assertEquals(responseVo.getData().getId(), 1);   //First Created
        Assert.assertEquals(responseVo.getData().getName(), name);
    }

    @Test
    public void givenCustomerIdReturnAssociatedSimCards() throws Exception {
        //Pre
        Customer customer = new Customer(1, "John", null);
        SimCard simCard1 = new SimCard(1, "sim1", customer);
        SimCard simCard2 = new SimCard(2, "sim2", customer);
        List<SimCard> simCards = Arrays.asList(simCard1, simCard2);
        customer.setSimCards(simCards);

        Mockito.when(customerService.getSims(customer.getId())).thenReturn(simCards);

        String uri = "/api/v1/customer/" + customer.getId() + "/sims";
        MockHttpServletResponse response = mvc.perform(MockMvcRequestBuilders
                .get(uri)
                .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        ResponseVo<List<SimCardVo>> responseVo = JsonConverter.convertToSims(response.getContentAsString());
        Assert.assertEquals(responseVo.getCode(), HttpStatus.OK.value());
        Assert.assertEquals(responseVo.getMessage(), Constants.RESPONSE_SUCCESS);
        Assert.assertEquals(responseVo.getData().size(), simCards.size());
    }

    @Test
    public void givenCustomerIdAndSimIdLinkSimWithCustomer() throws Exception {
        //Pre
        Customer customer = new Customer(1, "John", null);
        SimCard simCard = new SimCard(1, "sim1", null);
        Customer customerLinked = new Customer(1, "John", Collections.singletonList(simCard));
        Mockito.when(customerService.linkSimCard(customer.getId(), simCard.getId())).thenReturn(customerLinked);

        String uri = "/api/v1/customer/linkSim";
        MockHttpServletResponse response = mvc.perform(MockMvcRequestBuilders
                .put(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"customerId\":" + customer.getId() + ",\n" + "\"simId\":" + simCard.getId() + "}"))
                .andReturn()
                .getResponse();

        ResponseVo<CustomerVo> responseVo = JsonConverter.convertToCustomer(response.getContentAsString());
        Assert.assertEquals(responseVo.getCode(), HttpStatus.OK.value());
        Assert.assertEquals(responseVo.getMessage(), Constants.RESPONSE_SUCCESS);
        Assert.assertEquals(responseVo.getData().getSimCards().get(0).getId(), simCard.getId());
    }
}
