# Setup
- Import project as gradle project using any IDE (e.g. IntelliJ)
- Use _**bootRun**_ Task of Gradle to start the application
- Application URL : http://localhost:8080
- Please Read **```application.properties```** carefully to get the information about Database connectivity
- Database console URL : http://localhost:8080/h2
- Use tools like _**postman**_ to interact with the rest api

# REST API
##### 1. Create Customer 
- ```localhost:8080/api/v1/customer```
- ```POST```
- ```body: {"name" : "John"} (JSON)```
##### 2. Create Sim
- ```localhost:8080/api/v1/sim```
- ```POST```
- ```body: {"details": "SimCardDetails"}```
##### 3. Link sim to Customer
- ```localhost:8080/api/v1/customer/linkSim```
- ```PUT```
- ```body: {"customerId": 1, "simId":1}```
##### 4. Retrieve Customer Sims
- ```localhost:8080/api/v1/customer/1/sims```
- ```GET```

##
###### This code is not focused on business aspect of the telecommunication operator as Developer has no professional experience of Industry. However Developer has tried to satisfied the primary functions.